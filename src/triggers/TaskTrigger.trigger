trigger TaskTrigger on Task bulk(after insert, after update, after delete, after undelete) 
{
	// This trigger is for Honeywell Sensors and Control.

	List <Task> taskList;	
	List <Account> accountList = new List <Account>();	
	
	if (!Trigger.isDelete)
		taskList = Trigger.new;
		
	else
		taskList = Trigger.old;
							
	for (Task task : taskList)
	{
			Integer accCount = 0;
			Account acc;
				
				for (Account accQuery : [Select Id From Account where Id =: task.WhatId])
				{
					acc = accQuery; 
					accCount++;
				}
				
				if(accCount > 0 ) //For every Task that is created respective Account record type should be SS:Account RT and the owner of the account should be the owner of the current Task
				{
					acc.Last_Visit__c = task.ActivityDate;
					accountList.add(acc);
				}
			
		}
	if (accountList.size() > 0)
		update accountList;
}